package uz.pdp.appfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppSpringBootFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSpringBootFirstApplication.class, args);
    }

}
