package uz.pdp.appfirst.payload;

import lombok.Data;

@Data
public class FacultyDTO {

    private Integer id;

    private String name;

    private Integer universityId;
}
