package uz.pdp.appfirst.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Faculty {

    private Integer id;

    private String name;

    private University university;
}
