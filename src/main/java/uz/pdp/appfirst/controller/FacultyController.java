package uz.pdp.appfirst.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appfirst.model.Faculty;
import uz.pdp.appfirst.payload.FacultyDTO;
import uz.pdp.appfirst.service.FacultyService;

import java.util.Optional;

@RestController
@RequestMapping("/faculty")
@RequiredArgsConstructor
public class FacultyController {

    private final FacultyService facultyService;

    @PostMapping
    public HttpEntity<?> add(@RequestBody FacultyDTO facultyDTO) {
        //university not found
        //already exists
        //201
        Optional<Faculty> optionalFaculty = facultyService.create(facultyDTO);
        if (optionalFaculty.isEmpty())
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Oka xatolik");

        return ResponseEntity.status(HttpStatus.CREATED).body(optionalFaculty.get());
    }
}
