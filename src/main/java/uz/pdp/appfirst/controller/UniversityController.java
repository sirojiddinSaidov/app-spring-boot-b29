package uz.pdp.appfirst.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appfirst.model.University;
import uz.pdp.appfirst.service.UniversityService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/university")
@RequiredArgsConstructor
public class UniversityController {

    private final UniversityService universityService;


    @GetMapping
    public HttpEntity<List<University>> list() {
        return new ResponseEntity<>(universityService.all(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public HttpEntity<University> one(@PathVariable Integer id) {
        Optional<University> optionalUniversity = universityService.byId(id);

        if (optionalUniversity.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(optionalUniversity.get(), HttpStatus.OK);
    }


    @PostMapping
    public HttpEntity<?> add(@RequestBody University university) {
        Optional<University> optionalUniversity = universityService.create(university);
        if (optionalUniversity.isEmpty())
            return new ResponseEntity<>("OKa university qo'shilgan", HttpStatus.CONFLICT);

        return ResponseEntity.status(HttpStatus.CREATED).body(optionalUniversity.get());
    }


    @PutMapping("/{id}")
    public HttpEntity<University> update(@PathVariable Integer id,
                                         @RequestBody University university) {

        Optional<University> optionalUniversity = universityService.edit(id, university);

        if (optionalUniversity.isEmpty())
            return ResponseEntity.notFound().build();

        return ResponseEntity.accepted().body(optionalUniversity.get());
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> remove(@PathVariable Integer id) {
        boolean deleted = universityService.deleteById(id);
        if (!deleted)
            return ResponseEntity.notFound().build();

        return ResponseEntity.noContent().build();
    }
}
