package uz.pdp.appfirst.service;

import org.springframework.stereotype.Service;
import uz.pdp.appfirst.model.University;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UniversityService {

    private final List<University> universities = new ArrayList<>();


    public List<University> all() {
        return universities;
    }


    public Optional<University> byId(Integer id) {
        return universities.stream()
                .filter(university -> university.getId().equals(id))
                .findFirst();
    }

    public Optional<University> create(University university) {
        if (universities.stream()
                .anyMatch(one -> one.getName().equals(university.getName())))
            return Optional.empty();

        if (universities.isEmpty())
            university.setId(1);
        else
            university.setId(universities.get(universities.size() - 1).getId() + 1);

        universities.add(university);

        return Optional.of(university);
    }

    public Optional<University> edit(Integer id, University university) {
        return universities.stream().filter(one -> one.getId().equals(id))
                .peek(item -> item.setName(university.getName())).findFirst();
    }

    public boolean deleteById(Integer id) {
        return universities.removeIf(university -> university.getId().equals(id));
    }

}
