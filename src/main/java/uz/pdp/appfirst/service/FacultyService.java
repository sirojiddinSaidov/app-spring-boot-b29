package uz.pdp.appfirst.service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import uz.pdp.appfirst.model.Faculty;
import uz.pdp.appfirst.model.University;
import uz.pdp.appfirst.payload.FacultyDTO;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FacultyService {

    private final UniversityService universityService;

    private final List<Faculty> faculties = new LinkedList<>();

    public Optional<Faculty> create(FacultyDTO facultyDTO) {
        Optional<University> optionalUniversity = universityService.byId(facultyDTO.getUniversityId());

        if (optionalUniversity.isEmpty())
            return Optional.empty();

        boolean match = faculties.stream()
                .anyMatch(f ->
                        f.getName().equals(facultyDTO.getName())
                                && f.getUniversity().getId().equals(facultyDTO.getUniversityId()));
        if (match)
            return Optional.empty();

        Faculty faculty = Faculty.builder()
                .id(faculties.isEmpty() ? 1 : faculties.get(faculties.size() - 1).getId() + 1)
                .name(facultyDTO.getName())
                .university(optionalUniversity.get())
                .build();

        faculties.add(faculty);
        return Optional.of(faculty);
    }
}
